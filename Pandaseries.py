#Program to use pandas for data series
import pandas as pd
#Using tuples
y = ('a','b','c','1','2')
yp = pd.Series(y)
type(yp)
print yp

#Using list
x = [1,2,3,'a','b']
xp = pd.Series(x,index = ['a1','a2','a3','a4','a5'])
type(xp)
print xp[['a3','a1']]
 #Using key value
z = {'name': 'avi','age' : 23,'place': 'chennai'}
zp = pd.Series(z)
type(zp)
print zp

#Using Dataframes
m = {'name' : ['avi','mani','sai'], 'age': [23,12,27], 'place': ['chennai','vellore','madurai']}
md = pd.DataFrame(m)
type(md)
md.index = ['a1','a2','a3']
md.ix['a1']
md.ix['a1','age'] = 93
#del md.ix['a2']
del md['place']
md['address'] = ['tnagar','r.a puram','mylapore']
type(md)
print md
md = md.set_index(['Value'])
print md


