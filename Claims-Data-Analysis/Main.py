from pyspark.sql import SparkSession
import dataframes
import pyspark.sql.functions as F


def processTransformation(filepath):
    try:
        claimsdf = dataframes.dfClaimsHist.buildClaimsDataframe(filepath, spark)
        # Santamdf = claimsdf.filter(claimsdf.insurer_nm == "DISCOVERY INSURE")
        # count = Santamdf.count()
        # print(count)
        return claimsdf
    except Exception as exprocesTransform:
        print(errorformat("While creating Claims Dataframes", exprocesTransform))


def findClaimId(clmdf):
    try:
        InsCompanyName = input('Enter the name of the Insurance company : ')
        InsClaimId = input('Enter the Claim Id : ')
        ClaimDetl = clmdf.filter((F.col("insurer_nm") == InsCompanyName) & (F.col("claimd_id") == InsClaimId))
        ClaimDetl.show()
        print('vijay')
        spark = SparkSession \
            .builder \
            .appName('ClaimsDataAnalysis') \
            .config("spark.sql.broadcastTimeout", "50")
    except Exception as exprocesTransform:
        print(errorformat("While creating Claims Dataframes", exprocesTransform))


def errorformat(phase, exceptionLog):
    print('### Exception occurred in Main Program' + phase + '###')
    print(exceptionLog)
    print('######################################################')


if __name__ == "__main__":

    .config('spark.default.parallelism', '1') \
    .config('spark.yarn.queue', 'claims.data') \
    .getOrCreate()

    filepath = 'C:/Dev/Data/'
    claimdf = processTransformation(filepath)

    print("""
	Welcome to Claims Data Analysis

	[1] - Find claim details by Claim id.
	[2] - Find number of claims by Vehicle make and model.
	[3] - Find all claims for an Insurance company
	[4] - Exit
	""")
    action = input('What would you like to do today? (Enter a number) ')

    if action == '1':
        findClaimId(claimdf)
    elif action == '2':
        findClaimVehicle()
    elif action == '3':
        findClaimbyCompany()
    elif action == '4':
        exit()
    else:
        print('No valid choice was given, try again')
